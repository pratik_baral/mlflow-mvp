import os
import logging

from boto3.session import Session
import boto3

# ACCESS_KEY = os.environ['AWS_ACCESS_KEY'] 
# SECRET_KEY = os.environ['AWS_SECRET_KEY']

logger = logging.getLogger()

def download_file(path_to_s3, destination=os.getcwd()):
    '''
    this method will download the file specified into the destination of choice
    
    params:
        path_to_s3: the s3 path of the input file .eg. s3://bucket/path/to/file.7z
        destination: the destination folder. Default value is os.getcwd()
    returns:
        None
    '''
    s3 = boto3.resource('s3')
    
    splits = path_to_s3.split("/")
    bucket_name = splits[2]
    file_name = "/".join(splits[3:]) 
  
    your_bucket = s3.Bucket(bucket_name)
    try:

        your_bucket.download_file(file_name, destination)
    except Exception as e:
        error_message = f'''ERROR encountered when trying to download {path_to_s3} '''
        logger.error(error_message)
        print(e)
        raise ConnectionError(error_message) from e        
    logger.info(f"The file has been downloaded to {destination}")
    
    return None

def upload_file(source, destination):
    '''
    This method uploads a file to s3 bucket
    params:
        source: The path to the source file eg. /tmp/albert.pkl
        destination: the destination eg. s3://bucket/models/albert.pkl
    returns None
    '''

    s3 = boto3.resource('s3')

    splits = destination("/")
    bucket_name = splits[2]
    file_name = "/".join(splits[3:])

    your_bucket = s3.Bucket(bucket_name)
    try:
        your_bucket.upload_file(source, file_name)
    except Exception as e:
        error_message = f'''ERROR encountered when trying to upload to  {destination3} '''
        logger.error(error_message)
        raise ConnectionError(error_message) from e

    logger.info(f"the file {source} has been uploaded to {destination}")
    
    return None
