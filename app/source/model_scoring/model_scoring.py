"""
Docs

Developers:
Pratik Baral

Description:
a poc on iris mlops template for autdocs as well
"""
import pickle
import argparse
import datetime
import sys
import os
from sklearn.tree import DecisionTreeClassifier
import logging

import mlflow
import pandas as pd
import boto3

s3 = boto3.client('s3')

sys.path.append("..")
from utils.s3.s3utils import download_file, upload_file

mlflow.set_experiment('demo_scoring_run')
DESTINATION_BUCKET=os.environ['SANDBOX_DESTINATION_BUCKET']

def dummy_method(arg_1, arg2=2 ):
    """
    this method is awesome

    Parameters
    __________
    arg_1 : int
        a random argument
    arg_2 : str
        some string

    Returns
    -------
    params : str
    some random message
        
    """
    return "some random message"

if __name__ == '__main__':
    # Initialize the parser
    parser = argparse.ArgumentParser(
        description="Scoring Module for Iris Dataset"
    )

    # Add the parameters positional/optional
    parser.add_argument('-m', '--input_model', help = "s3 path to model", type = str)
    parser.add_argument('-d', '--data_path', help = "s3 path to MID", type = str)

    # Parse the arguments
    args = parser.parse_args()

    # download_file(args.data_path)
    data_file_name = args.data_path.split("/")[-1]
       
    # Read the data file
    df = pd.read_csv(args.data_path)

    # Load the model
    model_file_name = args.input_model.split("/")[-1]
    model_file_key = "/".join(args.input_model.split("/")[3:])

    with open(model_file_name, 'wb') as model_file:
        s3.download_fileobj(args.input_model.split("/")[2], f'{model_file_key}', model_file)

    with open(model_file_name, 'rb') as model_file:
        model = pickle.load(model_file)
    # Score the Input File
   
    df['results'] =model.predict(df.values)
    print(df.head())
    df.to_csv(f"s3://{DESTINATION_BUCKET}/aws_poc/scored_files/{str(datetime.datetime.now())}.csv", index = False)

