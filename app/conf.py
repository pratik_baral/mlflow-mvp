import os
import sys

sys.path.insert(0, os.path.abspath('source'))
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.coverage', 'sphinx.ext.napoleon']
