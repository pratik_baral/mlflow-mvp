
# SET UP THE FOLLOWING ENVIRONMENTAL VARIABLES FIRST:
# 	* AWS_ACCESS_KEY_ID
#	* AWS_SECRET_ACCESS_KEY
#   * AWS_SCORING_CLUSTER_NAME

AWS_SCORING_CLUSTER_NAME=cli-trial

# configure the ecs-cli 
ecs-cli configure profile --access-key $AWS_ACCESS_KEY_ID --secret-key $AWS_SECRET_ACCESS_KEY --profile-name $AWS_SCORING_CLUSTER_NAME 

# configure the cluster
ecs-cli configure --cluster $AWS_SCORING_CLUSTER_NAME --default-launch-type FARGATE --config-name $AWS_SCORING_CLUSTER_NAME --region us-west-2

# create a cluster
ecs-cli up --cluster-config $AWS_SCORING_CLUSTER_NAME --ecs-profile $AWS_SCORING_CLUSTER_NAME >> /tmp/tmp_file.fil

# retreive VPC ID, subnet 1 and subnet 2
AWS_VPC_ID=$(cat /tmp/tmp_file.fil| grep -Eo 'vpc-[0-9a-zA-Z]+')
cat /tmp/tmp_file.fil | grep -Eo 'subnet-[0-9a-zA-Z]+' >> /tmp/tmp_file.fil
AWS_SUB_NET_1=$(cat /tmp/tmp_file.fil | grep -Eo 'subnet-[0-9a-zA-Z]+' | head -n 1)
AWS_SUB_NET_2=$(cat /tmp/tmp_file.fil | grep -Eo 'subnet-[0-9a-zA-Z]+' | tail -n 1)

echo AWS_SUB_NET_1:$AWS_SUB_NET_1
echo AWS_SUB_NET_2:$AWS_SUB_NET_2

# Retreive default security group id 
AWS_SECURITY_GROUP_ID=$(aws ec2 describe-security-groups --filters Name=vpc-id,Values=$AWS_VPC_ID --region us-west-2 | grep -m 1 -Eo 'sg-[0-9a-zA-Z]+')

echo AWS_SECURITY_GROUP_ID:$AWS_SECURITY_GROUP_ID

[optional] open port for web service ingress
# aws ec2 authorize-security-group-ingress --group-id security_group_id --protocol tcp --port 80 --cidr 0.0.0.0/0 --region us-west-2

# create ecs-paramls.yml
echo "version: 1" > ecs-params.yml
echo "task_definition:" >>ecs-params.yml
echo "task_execution_role: ecsTaskExecutionRole" >> ecs-params.yml
echo "  ecs_network_mode: awsvpc" >> ecs-params.yml
echo "  os_family: Linux" >> ecs-params.yml
echo "  task_size:" >> ecs-params.yml
echo "    mem_limit: 0.5GB" >> ecs-params.yml
echo "    cpu_limit: 256" >> ecs-params.yml
echo "run_params:" >> ecs-params.yml
echo "  network_configuration:" >> ecs-params.yml
echo "    awsvpc_configuration:" >> ecs-params.yml
echo "      subnets:" >> ecs-params.yml
echo "        - \"$AWS_SUB_NET_1\"" >> ecs-params.yml
echo "        - \"$AWS_SUB_NET_2\"" >> ecs-params.yml
echo "      security_groups:" >> ecs-params.yml
echo "        - \"$AWS_SECURITY_GROUP_ID\"" >> ecs-params.yml
echo "      assign_public_ip: ENABLED" >> ecs-params.yml


# create docker-compose.yml template
echo "version: '3'" > docker-compose.yml
echo "services:" >> docker-compose.yml
echo "  web:" >> docker-compose.yml
echo "    image: amazon/amazon-ecs-sample" >> docker-compose.yml
echo "    ports:" >> docker-compose.yml
echo "      - "80:80"" >> docker-compose.yml
echo "    logging:" >> docker-compose.yml
echo "      driver: awslogs" >> docker-compose.yml
echo "      options: " >> docker-compose.yml
echo "        awslogs-group: write a log group" >> docker-compose.yml
echo "        awslogs-region: us-west-2" >> docker-compose.yml
echo "        awslogs-stream-prefix: web" >> docker-compose.yml

# run the task
ecs-cli compose --project-name $AWS_SCORING_CLUSTER_NAME service up --create-log-groups --cluster-config $AWS_SCORING_CLUSTER_NAME --ecs-profile $AWS_SCORING_CLUSTER_NAME 


# view container state (task state)
ecs-cli compose --project-name $AWS_SCORING_CLUSTER_NAME service ps --cluster-config $AWS_SCORING_CLUSTER_NAME --ecs-profile $AWS_SCORING_CLUSTER_NAME


# clean up
# ecs-cli compose --project-name $AWS_SCORING_CLUSTER_NAME service down --cluster-config $AWS_SCORING_CLUSTER_NAME --ecs-profile $AWS_SCORING_CLUSTER_NAME

# ecs-cli down --force --cluster-config $AWS_SCORING_CLUSTER_NAME --ecs-profile $AWS_SCORING_CLUSTER_NAME

