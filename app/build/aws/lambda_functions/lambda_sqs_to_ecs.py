import json
import urllib.parse
import boto3
import os
import logging

logging.basicConfig(level=logging.info)
logging.info('Loading the function')

ecs = boto3.client('ecs')

try:
    ecs_task = os.environ['ECS_TASK_TO_RUN']
    ecs_cluster = os.environ['FARGATE_CLUSTER_TO_RUN']
    model_path = os.environ['MODEL_PATH']
    destination_bucket = os.environ['SANDBOX_DESTINATION_BUCKET']
except Exception as e:
    logging.error("""
    Error while retreiving the environmental variables 
    Please Ensure that you have set the following environmental variables to appropriate values:
        * ECS_TASK_TO_RUN: the name of the ecs task definition (i.e. docker container) that needs to be triggered
        * FARGATE_CLUSTER_TO_RUN: the name of the cluster where the task definition is stored
        * MODEL_PATH: path to the model artifact. eg. s3://public-models/models/iris-model.pkl
        * SANDBOX_DESTINATION_BUCKET: the destination bucket to store scored files. eg. "scored-bucket" for s3://scored-bucket
        """)
    
    
def lambda_handler(event, context):
    for record in event['Records']:
        
        print("test")
        print(record)
        bucket = record["body"]
        key = record['messageAttributes']['key']['stringValue']
        s3_uri = "s3://"+bucket+"/"+key
        print(f"key is {key}")

    
        # Run the ECS Task i.e. the docker container
        logging.info("running ECS Task")
        logging.debug(f"s3 uri of input file: s3://{bucket}/{key}")
        
    
        response = ecs.run_task(
            cluster=ecs_cluster,
            taskDefinition=ecs_task,
            count=1,
            launchType='FARGATE',
            networkConfiguration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-07d5b7ef1326d9800'
                    ],
                    
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
            'containerOverrides': [
                {
                    'name': "pratik-dockerhub-iris",
                    'environment': [
                        {
                            'name': 'MODEL_PATH',
                            'value': model_path
                        },
                        {
                            'name': 'DATA_PATH',
                            'value': s3_uri
                           
                        },
                        {
                             'name': 'SANDBOX_DESTINATION_BUCKET',
                             'value': destination_bucket
                        }
                        
                    ],
                    'cpu': 1,
                    'memory': 256,
                }
                    ]
         
                    },
            )
        logging.info(response)
