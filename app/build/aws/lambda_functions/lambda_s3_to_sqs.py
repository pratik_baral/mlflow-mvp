import json
import urllib.parse
import boto3
import os
import logging

logging.basicConfig(level=logging.info)
logging.info('Loading function')

s3 = boto3.client('s3')
sqs = boto3.client('sqs', endpoint_url='https://sqs.us-west-2.amazonaws.com')

try:
    ecs_task = os.environ['ECS_TASK_TO_RUN']
    ecs_cluster = os.environ['FARGATE_CLUSTER_TO_RUN']
    model_path = os.environ['MODEL_PATH']
    destination_bucket = os.environ['SANDBOX_DESTINATION_BUCKET']
    queue_url = os.environ['AWS_QUEUE_URL']
except Exception as e:
    logging.error("""
    Error while retreiving the environmental variables 
    Please Ensure that you have set the following environmental variables to appropriate values:
        * ECS_TASK_TO_RUN: the name of the ecs task definition (i.e. docker container) that needs to be triggered
        * FARGATE_CLUSTER_TO_RUN: the name of the cluster where the task definition is stored
        * MODEL_PATH: path to the model artifact. eg. s3://public-models/models/iris-model.pkl
        * SANDBOX_DESTINATION_BUCKET: the destination bucket to store scored files. eg. "scored-bucket" for s3://scored-bucket
        """)
        

def lambda_handler(event, context):
    '''
    This method is invoked when a new mid file is created on the bucket that triggers the lambda method
    params:
        event: the event that trigerred the lambda
        context: the meta-data about the event
    
    returns:
        None

    '''
    
    # Get the bucket and the key for the newly created object
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    s3_uri = "s3://"+bucket+'/'+key
    
    # ensure that the destination and source bucket are different to avoid recursion
    if bucket==destination_bucket:
        logging.error(f"Source and Destination Bucket are same- {bucket}")
        raise Exception("data_source and output_destination bucket are the same. Use different buckets")
        
    # if the file name contains spaces, those will be renamed with spaces replaced by underscore ("_") 
    if " " in s3_uri:
        s3_uri = s3_uri.replace(" ", "_")
        copy_source = {'Bucket': bucket, 'Key': key}
        s3.copy_object(CopySource = copy_source, Bucket = bucket, Key = key.replace(" ", "_"))
        s3.delete_object(Bucket = bucket, Key = key)
        return
    
    # Send message to SQS queue
    response = sqs.send_message(
        QueueUrl=queue_url,
        DelaySeconds=1,
        MessageAttributes={
            'key': {
                'DataType': 'String',
                'StringValue': key
            },
            'bucket': {
                'DataType': 'String',
                'StringValue': bucket
            }
        },
        MessageBody=(bucket)
        )
    logging.info("message queued")

    return 
